
Ref#1: 
Learning Face Age Progression: A Pyramid Architecture of GANs
https://arxiv.org/abs/1711.10352
https://arxiv.org/pdf/1711.10352.pdf


Abstract

Hongyu Yang, Di Huang, Yunhong Wang, Anil K. Jain
(Submitted on 28 Nov 2017)

The two underlying requirements of face age progression, i.e. aging accuracy and identity permanence, are not well handled in the literature. In this paper, we present a novel generative adversarial network based approach. It separately models the constraints for the intrinsic subject-specific characteristics and the age-specific facial changes with respect to the elapsed time, ensuring that the generated faces present desired aging effects while simultaneously keeping personalized properties stable. Further, to generate more lifelike facial details, high-level age-specific features conveyed by the synthesized face are estimated by a pyramidal adversarial discriminator at multiple scales, which simulates the aging effects in a finer manner. The proposed method is applicable for diverse face samples in the presence of variations in pose, expression, makeup, etc., and remarkably vivid aging effects are achieved. Both visual fidelity and quantitative evaluations show that the approach advances the state-of-the-art. 

    Subjects: 	Computer Vision and Pattern Recognition (cs.CV)
    Cite as: 	arXiv:1711.10352 [cs.CV]
      	(or arXiv:1711.10352v1 [cs.CV] for this version)
	Submission history
	From: Hongyu Yang [view email]
	[v1] Tue, 28 Nov 2017 15:42:46 GMT (9526kb,D)
	Which authors of this paper are endorsers? | Disable MathJax (What is MathJax?)



Ref#2:
GAN: A Beginner’s Guide to Generative Adversarial Networks
https://deeplearning4j.org/generative-adversarial-network
https://github.com/deeplearning4j/deeplearning4j
Note: it has a lot of references with art use cases


Ref#3
A list of all named GANs! 
https://github.com/hindupuravinash/the-gan-zoo
by: Avinash Hindupur
http://hindupuravinash.com/
https://deephunt.in/

